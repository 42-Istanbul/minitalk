#include <stdlib.h>
#include <unistd.h>

int main(void)
{
	char *arr;

	arr = (char *)malloc(sizeof(char) * 4);
	arr[0] = 0b11110000;
	arr[1] = 0b10011111;
	arr[2] = 0b10010011;
	arr[3] = 0b10100010;

	write(1, &arr[0], 1);
	write(1, &arr[1], 1);
	write(1, &arr[2], 1);
	write(1, &arr[3], 1);
}
