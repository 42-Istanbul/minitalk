#include <stdio.h>
#include "minitalk.h"

void	my_handler(int signum, siginfo_t *siginfo, void *gbg)
{
	static int	out_char;
	static int	pow = 0;

	if (!out_char)
		out_char = 0;
	(void)gbg;
	if (signum == SIGUSR1)
	{
		out_char += 1 << (7 - pow);
		ft_printf("[%d] ", pow);
	}
	else
		ft_printf("%d ", pow);
	pow += 1;
	if (pow == 8)
	{
		ft_printf("\nout=%d -> %c\n", out_char, out_char);
		out_char = 0;
		pow = 0;
		ft_printf("client=%d\n", siginfo->si_pid);
		if (kill(siginfo->si_pid, SIGUSR2) == -1)
			handle_error("Signal error!");
	}
}

int	main(void)
{
	struct sigaction	sig;

	ft_printf("Server PID: %d\n", getpid());
	sig.sa_flags = SA_SIGINFO;
	sig.sa_sigaction = my_handler;
	if ((sigaction(SIGUSR1, &sig, 0)) == -1)
		handle_error("Signal error!");
	if ((sigaction(SIGUSR2, &sig, 0)) == -1)
		handle_error("Signal error!");
	while (1)
		pause();
	return (0);
}
