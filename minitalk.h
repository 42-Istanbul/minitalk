#ifndef MINITALK_H
# define MINITALK_H

# include <signal.h>
# include <stdlib.h>
# include "./ft_printf/ft_printf.h"

# define STDIN 0
# define STDOUT 1
# define STDERR 2
# define MAX_PID 99998

void	handle_error(char *s);

#endif
